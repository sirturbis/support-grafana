#!/bin/bash -e

sleep 120
curl -v --basic -X POST "admin:$GF_SECURITY_ADMIN_PASSWORD@localhost:3000/api/plugins/alexanderzobnin-zabbix-app/settings?enabled=true" -d ''
curl -v --basic -X POST "admin:$GF_SECURITY_ADMIN_PASSWORD@localhost:3000/api/admin/users" -d "username=support&login=support&email=info@lukapo.com&password=support@2020&active=true,approved=true"
curl -v --basic -X POST -H "Content-type: application/json" "admin:$GF_SECURITY_ADMIN_PASSWORD@localhost:3000/api/playlists/" -d '{"name": "SUPPORT",
    "interval": "1m",
    "items": [
      {
        "type": "dashboard_by_id",
        "value": "1",
        "order": 1,
        "title":"SUPPORT-ALERTS"
      },
      {
        "type": "dashboard_by_id",
        "value": "2",
        "order": 2,
        "title":"SUPPORT-TICKETS-BY-CUSTOMERS"
      },
      {
        "type": "dashboard_by_id",
        "value": "3",
        "order": 3,
        "title":"SUPPORT-TICKETS"
      }
      ,
      {
        "type": "dashboard_by_id",
        "value": "4",
        "order": 4,
        "title":"SUPPORT-ZABBIX"
      }
    ]
  }'



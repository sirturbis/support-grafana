FROM grafana/grafana:7.0.3
USER root

LABEL maintainer="Ondřej Tůma <sirTurbis@gmail.com>"

COPY provisioning /etc/grafana/provisioning
COPY dashboards /var/lib/grafana/dashboards

COPY extented-run.sh /extented-run.sh
RUN chown -R grafana:grafana  /extented-run.sh && \
    chmod +x /extented-run.sh && \
    sed -i '/^exec grafana-server.*/i /extented-run.sh &' /run.sh && \
    apk add curl

